// Supported with union (c) 2018-2023 Union team

// User API for zCModel
// Add your methods here

int Hook_SetMeshLibTexture(const zSTRING& meshLibName, const int channel, const int varNr, zSTRING* texNamePart);
int Hook_RemoveMeshLib(const zSTRING& meshLibName);
void Hook_RemoveMeshLibAll();
void dtor__hook();

int SetMeshLibTexture_Union(const zSTRING& meshLibName, const int channel, const int varNr, zSTRING* texNamePart);
int RemoveMeshLib_Union(const zSTRING& meshLibName);