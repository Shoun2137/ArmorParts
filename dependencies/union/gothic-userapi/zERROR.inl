// Supported with union (c) 2018-2023 Union team

// User API for zERROR
// Add your methods here

int Report_hook(zERROR_TYPE type, int id, const zSTRING& str_text, signed char levelPrio, unsigned int flag, int line, char* file, char* function);
