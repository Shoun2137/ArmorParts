// Supported with union (c) 2018-2023 Union team

// User API for oCNpc
// Add your methods here

void Hook_InitModel();
void Hook_UnequipItem(oCItem* item);
void Hook_EquipArmor(oCItem* item);
void Unarchive_Hook(zCArchiver& Archive);

void RefreshModel();
void UnequipItem_Union(oCItem* item);
void EquipArmor_Union(oCItem* item);

inline zCArray<oCItem*> GetEquippedArmorParts() const;

TNpcSlot* CreateArmorPartSlot(const zSTRING& name);
void PutArmorPartInSlot(oCItem* item);
void RemoveArmorPartFromSlot(oCItem* item);

void UnequipArmorPartFromWear(int wear);
void RemoveArmorPartFromMeshLib(oCItem* item);
bool CanEquipArmorPart(oCItem* item);
