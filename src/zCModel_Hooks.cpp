#include "Definitions.h"

auto Hook_zCModel_SetMeshLibTexture = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x00564150, 0x0057E1D0)),
	&zCModel::Hook_SetMeshLibTexture,
	Union::HookType::Hook_Detours
);

int zCModel::Hook_SetMeshLibTexture(const zSTRING& meshLibName, const int channel, const int varNr, zSTRING* texNamePart)
{
	Hook_zCModel_SetMeshLibTexture.Disable();

	int result = SetMeshLibTexture_Union(meshLibName, channel, varNr, texNamePart);

	Hook_zCModel_SetMeshLibTexture.Enable();

	return result;
}

auto Hook_zCModel_RemoveMeshLib = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x00564380, 0x0057E400)),
	&zCModel::Hook_RemoveMeshLib,
	Union::HookType::Hook_Detours
);

int zCModel::Hook_RemoveMeshLib(const zSTRING& meshLibName)
{
	Hook_zCModel_RemoveMeshLib.Disable();

	int result = RemoveMeshLib_Union(meshLibName);

	Hook_zCModel_RemoveMeshLib.Enable();

	return result;
}
