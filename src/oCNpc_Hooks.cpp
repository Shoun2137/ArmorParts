#include "Definitions.h"

auto Hook_oCNpc_InitModel = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x00695020, 0x00738480)),
	&oCNpc::Hook_InitModel,
	Union::HookType::Hook_Detours
);

void oCNpc::Hook_InitModel() 
{
	Hook_oCNpc_InitModel.Disable();

	RefreshModel();

	Hook_oCNpc_InitModel.Enable();
}

auto Hook_oCNpc_UnequipItem = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x0068FBC0, 0x007326C0)),
	&oCNpc::Hook_UnequipItem,
	Union::HookType::Hook_Detours
);

void oCNpc::Hook_UnequipItem(oCItem* item)
{
	Hook_oCNpc_UnequipItem.Disable();

	UnequipItem_Union(item);

	Hook_oCNpc_UnequipItem.Enable();
}

auto Hook_oCNpc_EquipArmor = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x00697080, 0x0073A490)),
	&oCNpc::Hook_EquipArmor,
	Union::HookType::Hook_Detours
);

void oCNpc::Hook_EquipArmor(oCItem* item) 
{
	Hook_oCNpc_EquipArmor.Disable();

	EquipArmor_Union(item);

	Hook_oCNpc_EquipArmor.Enable();
}
