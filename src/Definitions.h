#include <Union/Hook.h>
#include <ZenGin/zGothicAPI.h>

#if __G1
using namespace Gothic_I_Classic;
#elif __G1A
using namespace Gothic_I_Addon;
#elif __G2
using namespace Gothic_II_Classic;
#elif __G2A
using namespace Gothic_II_Addon;
#endif

static const int ITM_WEAR_EFFECT = 16;

static zSTRING SlotPrefix = "ZS_WEAR_"; // In Daedalus scripts wear "flags" start at 64.
static zSTRING SoftSkinWearSeparator = "  ";

inline zSTRING GetSoftSkinSlotName(int wear) 
{
	return SlotPrefix + zSTRING(wear);
}

inline bool IsSoftSkinSlot(const zSTRING& name) 
{
	return name.Copied(0, SlotPrefix.Length()) == SlotPrefix;
}

inline bool IsArmorPart(oCItem* item)
{
	return item && item->wear > ITM_WEAR_EFFECT && item->HasFlag(ITM_CAT_ARMOR) && !(item->wear & ITM_WEAR_TORSO);
}
