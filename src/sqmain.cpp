#if __G2A && SCRAT_EXPORT

#include <Externals.h>
#include <sqapi.h>

inline oCNpc* getPlayerPtr(int idx)
{
    Sqrat::Function func = Sqrat::RootTable().GetFunction("getPlayerPtr");
    if (func.IsNull())
        return nullptr;

    Sqrat::SharedPtr<SQUserPointer> result = func.Evaluate<SQUserPointer>(idx);
    if (result.Get() == nullptr)
        return nullptr;

    return reinterpret_cast<oCNpc*>(*result);
}

void sq_insertSoftSkinModel(int playerId, char* visualName, int fakeSlot)
{
    oCNpc* npc = getPlayerPtr(playerId);
    if (npc)
        insertSoftSkinModel(zSTRING(visualName), npc, fakeSlot);
}

void sq_removeSoftSkinModel(int playerId, int fakeSlot)
{
    oCNpc* npc = getPlayerPtr(playerId);
    if (npc)
        removeSoftSkinModel(npc, fakeSlot);
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

    Sqrat::RootTable(vm)
        .Func("insertSoftSkinModel", &sq_insertSoftSkinModel)
        .Func("removeSoftSkinModel", &sq_removeSoftSkinModel);

	Game_DefineExternals();
	return SQ_OK;
}
#endif