#ifndef _HACKS_MEMORY_PATCH
#define _HACKS_MEMORY_PATCH

#include <cstring>
#include <vector>
#include <windows.h>

typedef struct untyped__
{
	void* data;

	template <class T>
	untyped__(T x)
	{
		data = *reinterpret_cast<void**>(&x);
	}

	operator void* () const
	{
		return data;
	}

} untyped;

class MemoryPatch
{
private:
	void Protect(const untyped target, DWORD& protection)
	{
		VirtualProtect(target, 5, protection, &protection);
		protection = 0;
	}

	void Unprotect(const untyped target, DWORD& protection)
	{
		VirtualProtect(target, 5, PAGE_EXECUTE_READWRITE, &protection);
	}

	template <class T>
	void Apply(const untyped target, T data, size_t size)
	{
		DWORD protection = 0;

		Unprotect(target, protection);
		memcpy(target, (void*)data, size);
		Protect(target, protection);

		// flushing CPU cache
		FlushInstructionCache(GetCurrentProcess(), NULL, NULL);
	}

public:
	MemoryPatch(const untyped target, int nop_size) //for noping
	{
		std::vector<BYTE> bytes(nop_size, 0x90);
		Apply(target, &bytes[0], bytes.size());
	};

	MemoryPatch(const untyped target, std::vector<BYTE> bytes)
	{
		Apply(target, &bytes[0], bytes.size());
	};

	template <typename T>
	MemoryPatch(const untyped target, T data)
	{
		Apply(target, &data, sizeof(data));
	};
};


//#if __G1
//MemoryPatch patch_oCNpc_InitModel_call_zCModel_RemoveMeshLibAll(0x0069511D, { 0x90, 0x90, 0x90, 0x90, 0x90 });
//MemoryPatch patch_oCNpc_EquipArmor_disable_item_wear_error(0x006971F3, { 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90 });
//#elif __G2A
//MemoryPatch patch_oCNpc_InitModel_call_zCModel_RemoveMeshLibAll(0x0073857D, { 0x90, 0x90, 0x90, 0x90, 0x90 });
//MemoryPatch patch_oCNpc_EquipArmor_disable_item_wear_error(0x0073A620, { 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90 });
//#endif

#if __G1
MemoryPatch nop_oCNpc_InitModel_call_zCModel_RemoveMeshLibAll(0x0069511D, 5);
MemoryPatch nop_oCNpc_EquipArmor_disable_item_wear_error(0x006971F3, 39);
#elif __G2A
MemoryPatch nop_oCNpc_InitModel_call_zCModel_RemoveMeshLibAll(0x0073857D, 5);
MemoryPatch nop_oCNpc_EquipArmor_disable_item_wear_error(0x0073A620, 39);
#endif

#endif