#include "Definitions.h"

void oCNpc::RefreshModel()
{
    auto model = GetModel();
    if (!model)
        return;

    model->RemoveMeshLibAll();

    auto armors = GetEquippedArmorParts();
    for (int i = 0; i < armors.GetNumInList(); ++i)
    {
        if (!armors[i])
            continue;

        zSTRING visualName = armors[i]->GetVisualChange();
        if (!visualName.IsEmpty()) 
        {
            model->ApplyMeshLib(visualName);
            model->SetMeshLibTexture_Union(visualName, 0, body_TexVarNr, &zSTRING("BODY"));
            model->SetMeshLibTexture_Union(visualName, 1, body_TexColorNr, &zSTRING("BODY"));
            model->SetMeshLibTexture_Union(visualName, 0, armors[i]->visual_skin, &zSTRING("ARMOR"));
        }
    }

    InitModel();
}

void oCNpc::UnequipItem_Union(oCItem* item) 
{
    if (!item || !item->HasFlag(ITM_FLAG_ACTIVE))
        return;

#if __G1
    if (item->wear & ITM_WEAR_TORSO && item->wear > ITM_WEAR_TORSO)
    {
        oCItem* unequipped = RemoveFromSlot(NPC_NODE_TORSO, TRUE)->CastTo<oCItem>();
        RemoveItemEffects(unequipped);
        return;
    }
#endif

    if (IsArmorPart(item))
    {
        UnequipItem(item);
        RemoveArmorPartFromSlot(item);
        return;
    }

    UnequipItem(item);
}

void oCNpc::EquipArmor_Union(oCItem* item)
{
    if (!item)
        return;

    if (item->HasFlag(ITM_FLAG_ACTIVE))
    {
        UnequipItem(item);
        return;
    }

    if (IsAPlayer())
    {
        if (!CanUse(item))
        {
            DisplayCannotUse();
            return;
        }

        if (!CanEquipArmorPart(item))
        {
            ogame->game_text->Printwin(
                zSTRING("Item '") + 
                GetEquippedArmor()->name + 
                zSTRING("' is incompatible with '") + 
                item->name + 
                zSTRING("'!\n")
            );
            return;
        }
    }

    UnequipArmorPartFromWear(item->wear);

#if __G1
    if (item->wear & ITM_WEAR_TORSO && item->wear > ITM_WEAR_TORSO && !item->HasFlag(ITM_FLAG_ACTIVE))
    {
        UnequipItem(GetSlotItem(NPC_NODE_TORSO));
        EquipItem(item);
        PutInSlot(NPC_NODE_TORSO, item, TRUE);
        return;
    }
#endif
     EquipArmor(item);

    if (IsArmorPart(item) && !item->HasFlag(ITM_FLAG_ACTIVE))
    {
        EquipItem(item);
        PutArmorPartInSlot(item);
        RefreshModel();
    }
}

inline zCArray<oCItem*> oCNpc::GetEquippedArmorParts() const
{
    zCArray<oCItem*> armors;

#if __G2A
    auto contList = inventory2.inventory.next;
#else
    auto contList = inventory2.inventory[INV_ARMOR].next;
#endif

    while (contList) 
    {
        oCItem* item = contList->data;
        if (IsArmorPart(item) && item->HasFlag(ITM_FLAG_ACTIVE))
            armors.Insert(item);

        contList = contList->next;
    }

    return armors;
}

void oCNpc::UnequipArmorPartFromWear(int wear) 
{
    if (wear <= ITM_WEAR_LIGHT)
        return;

    for (int i = 0; i < invSlot.GetNumInList(); ++i) 
    {
        TNpcSlot* slot = invSlot[i];
        oCItem* item = slot->vob->CastTo<oCItem>();

        if (!item || !IsSoftSkinSlot(slot->name))
            continue;

        if ((item->wear & wear) > ITM_WEAR_EFFECT)
            UnequipItem_Union(item);
    }
}


void oCNpc::RemoveArmorPartFromMeshLib(oCItem* item) 
{
    zCModel* model = GetModel();
    if (!model)
        return;

    zSTRING visualName = item->GetVisualChange();
    if (!visualName.IsEmpty())
        model->RemoveMeshLib_Union(visualName);
}

TNpcSlot* oCNpc::CreateArmorPartSlot(const zSTRING& slotname)
{
    TNpcSlot* slot = GetInvSlot(slotname);
    if (slot)
        return slot;

    CreateInvSlot(slotname);
    return GetInvSlot(slotname);
}

void oCNpc::PutArmorPartInSlot(oCItem* item) 
{
    if (!item)
        return;

    zSTRING slotName = GetSoftSkinSlotName(item->wear);
    TNpcSlot* slot = CreateArmorPartSlot(slotName);
    PutInSlot(slot, item, TRUE);
}

void oCNpc::RemoveArmorPartFromSlot(oCItem* item) 
{
    TNpcSlot* slot = GetInvSlot(GetSoftSkinSlotName(item->wear));

#if __G1
    oCVob* vob = RemoveFromSlot(slot, FALSE);
#elif __G2A
    oCVob* vob = RemoveFromSlot(slot, FALSE, TRUE);
#endif

    RefreshModel();
}

bool oCNpc::CanEquipArmorPart(oCItem* item)
{
    if (item->wear & ITM_WEAR_TORSO)
        return true;

    oCItem* armor = GetEquippedArmor();

    if (!armor)
        return true;

    return (armor->wear & item->wear) != item->wear;
}
