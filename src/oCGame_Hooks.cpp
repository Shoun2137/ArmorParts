#include "Externals.h"

auto Hook_oCGame_DefineExternals_Ulfi = Union::CreateHook(
	reinterpret_cast<void*>(zSwitch(0x006495B0, 0x006D4780)),
	&oCGame::Hook_DefineExternals_Ulfi,
	Union::HookType::Hook_Detours
);
void oCGame::Hook_DefineExternals_Ulfi(zCParser* parser)
{
	Hook_oCGame_DefineExternals_Ulfi.Disable();
	DefineExternals_Ulfi(parser);

	Game_DefineExternals();
}