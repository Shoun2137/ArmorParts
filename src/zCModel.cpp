#include "Definitions.h"

int zCModel::SetMeshLibTexture_Union(const zSTRING& meshLibName, const int channel, const int varNr, zSTRING* texNamePart) {
	
	zSTRING nameNoExt = meshLibName;
	nameNoExt = nameNoExt.Upper().PickWord(1, ".", ".");

	int applies = 0;
	for (int i = 0; i < meshLibList.GetNum(); ++i) 
	{
		if (meshLibList[i]->meshLib->objectName == nameNoExt) {
			meshLibList[i]->texAniState.SetChannelVariation(channel, varNr, texNamePart);
			applies++;
		}
	}
	return applies;
}

int zCModel::RemoveMeshLib_Union(const zSTRING& meshLibName) 
{
	zSTRING nameNoExt = meshLibName;
	nameNoExt = nameNoExt.Upper().PickWord(1, ".", ".");

	int removes = 0;

	for (int i = 0; i < meshLibList.GetNum(); ++i) 
	{
		auto& meshLibListNode = meshLibList[i];
		if (meshLibListNode->meshLib->objectName == nameNoExt) {
			zCModelMeshLib* meshLib = meshLibListNode->meshLib;
			delete meshLibListNode;
			meshLibList.RemoveOrderIndex(i--);
			meshLib->RemoveFromModel(this);
			meshLib->Release();
			removes++;
		}
	}

	return removes;
}
