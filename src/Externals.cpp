#include "Definitions.h"

//TODO: Think which way to proceed further:

// a) "Proper":
//    Primitive softskin insertion (same as below) + Custom textureframe tracking/context 
//    switching at the end of zCModel::Render = Essentially reimplementation of zCModelTexAniState,
//    but without fucking static arrays. Armors could have unlimited materials this way.
//    There could be more ways to optimize things better than piranha did.

// b) "Dirty":
//    PartialHook into inlines inside zCModel::Render to acquire proper access into existing
//    zCModelTexAniState side so that we could use existing ApplyMeshlib/RemoveMeshlib impl.
//    throughout the plugin. This has one big con: All softskin-like armors right now can use
//    only 4 materials, it'll just override softskin if more are present in it.

// Note: This concerns only G2O side of things.

// This below is just a quick way to not depend on internal zengin equipment system from the armor side.

void insertSoftSkinModel(zSTRING visualName, oCNpc* npc, int fakeSlot)
{
    visualName.Overwrite(visualName.Upper().SearchRev(".ASC", 1), ".MDM");

    zCModelMeshLib* meshLib = new zCModelMeshLib();
    zCModelMeshLib::LoadMDM(visualName, NULL, npc->GetModel(), &meshLib);

    auto& thisSSList = meshLib->meshSoftSkinList;
    for (int i = 0; i < thisSSList.GetNum(); ++i)
    {
        zCMeshSoftSkin* softSkin = thisSSList[i];
        softSkin->AddRef();
        softSkin->objectName = visualName + SoftSkinWearSeparator + GetSoftSkinSlotName(fakeSlot);
        npc->GetModel()->meshSoftSkinList.Insert(softSkin);
    }
    meshLib->Release();

    npc->RefreshModel();
}

void removeSoftSkinModel(oCNpc* npc, int fakeSlot)
{
    auto& npcSSList = npc->GetModel()->meshSoftSkinList;

    for (int i = 0; i < npcSSList.GetNum(); ++i)
    {
        if (npcSSList[i]->GetObjectName().SearchRev(SoftSkinWearSeparator + GetSoftSkinSlotName(fakeSlot), 1) != -1)
        {
            npcSSList[i]->Release();
            npcSSList.RemoveOrderIndex(i--);
        }
    }

    npc->RefreshModel();
}

int Mdl_ApplySoftSkinItem() 
{
    int itemSymbolIdx;
    parser->GetParameter(itemSymbolIdx);
    oCNpc* npc = (oCNpc*)parser->GetInstance();

    if (npc)
    {
        oCItem* item = zfactory->CreateItem(itemSymbolIdx);
        insertSoftSkinModel(item->visual_change, npc, item->wear);
        item->Release();
    }
    return 0;
}

int Mdl_RemoveSoftSkinItem() 
{
    int fakeSlot;
    parser->GetParameter(fakeSlot);

    oCNpc* npc = (oCNpc*)parser->GetInstance();
    if (npc)
        removeSoftSkinModel(npc, fakeSlot);

    return 0;
}

void Game_DefineExternals() 
{
    parser->DefineExternal("Mdl_ApplySoftSkinItem", Mdl_ApplySoftSkinItem, zPAR_TYPE_VOID, zPAR_TYPE_INSTANCE,  zPAR_TYPE_INT, 0);
    parser->DefineExternal("Mdl_RemoveSoftSkinItem", Mdl_RemoveSoftSkinItem, zPAR_TYPE_VOID, zPAR_TYPE_INSTANCE, zPAR_TYPE_INT, 0);
    
    //IMO: Those daedalus externals should be deleted because their useless on their own.
    //     It's useful only for debugging.

    //TODO: GetEquippedArmorPartByWear()
}