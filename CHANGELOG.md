### Changelog

- Added squirrel function `insertSoftSkinModel`
- Added squirrel function `removeSoftSkinModel`
- Fixed bug with skipping model removal in daedalus externals