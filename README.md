# Armor Parts

### Introduction

ArmorParts is a DLL module for G1, G2:NOTR, and G2O. It enhances the original equipment system, allowing for the equipping of multiple parts, rather than just a single piece of armor.

**This module is intended solely for zengin modder/server developer use.**\
**This module does not include G2O player synchronization. You will need to create your own implementation.**\
Check folder `examples` for basic usage of the module.

ArmorParts is a hardfork of the union plugin called [zImprovedArmor](https://github.com/Gratt-5r2/zImprovedArmor).\
For more info about the original plugin, sample item scripts and models can be found in [this topic](https://worldofplayers.ru/threads/42225/).

### Usage

In order to use the module, the user have to install: \
[Microsoft Visual C++ 2015-2022 Redistributable (x86)](https://aka.ms/vs/17/release/vc_redist.x86.exe)\
For generic information about G2O modules, refer to [G2O Docs](https://gothicmultiplayerteam.gitlab.io/docs/0.3.0/module-manual/about/)\
For information related to the Union API, refer to [Union API wiki](https://gitlab.com/union-framework/union-api/-/wikis/home)\
For information related to the Gothic API, refer to [Gothic API wiki](https://gitlab.com/union-framework/gothic-api/-/wikis/home)

#### How to load your module:
* [Check G2O Docs](https://gothicmultiplayerteam.gitlab.io/docs/0.3.0/server-manual/configuration/#module)
* Union  
    * Put ArmorParts.dll inside `Gothic/System/Autorun`
    * Create VDFS archive with `System/Autorun` folder structure, put ArmorParts.dll inside it \
    then move VDFS archive to `Gothic/Data` 

### Compilation
#### Prerequisites

- [Visual Studio 2022](https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=Community&channel=Release&version=VS2022&source=VSLandingPage&passive=false)
- Windows SDK + toolset v142

#### Building with Visual Studio with CMake tools

- Open a local folder using Visual Studio
- Build union-api.sln
    - Choose `LIB|x86` for Release
    - Choose `LIBd|x86` for Debug
- Build ArmorParts.dll
    - Choose `G1-Release` or `G1-Debug` for Gothic 1
    - Choose `G2A-Release` or `G2A-Debug` for Gothic 2 Night of the Raven + Gothic 2 Online
- ???
- Profit
